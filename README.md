# ABP Apply Thread Icon

Allows moderators to apply a thread icon to thread(s) without having to edit the first post of that thread.

To use, you just select the Apply Icon Prefix option from the Moderation Options menu in a thread or from the Inline Thread Moderation in a forum, select the desired icon and click Apply Thread Icon.

Inspired by [Apply Thread Prefix](http://community.mybb.com/mods.php?action=view&pid=159) by Starpaul20

*English and French languages*